package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
)

var (
	connFile             = flag.String("file", "", "connections list file path")
	debugFlag            = flag.Bool("debug", false, "debug mode")
	connectionStringList = make([]string, 0, 0)
	lastIndex            = 0
)

func readConnectionListFile(path string) []string {
	file, err := os.Open(path)
	if err != nil {
		log.Fatalf("Error: %v\n", err)
		os.Exit(1)
	}

	defer file.Close()

	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			break
		}
		if line == "\n" || line == "" {
			continue
		}
		connectionStringList = append(connectionStringList, strings.TrimSpace(line))
	}

	return connectionStringList
}

func main() {
	flag.Parse()

	connectionStringList = readConnectionListFile(*connFile)

	http.HandleFunc("/", connectionHandler)

	fmt.Println("Serve on 0.0.0.0:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func connectionHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "text/plain")
	fmt.Fprintf(w, "%v", roundRobin(connectionStringList, &lastIndex))
}

func roundRobin(list []string, lastIndex *int) string {
	*lastIndex = (*lastIndex + 1) % len(list)
	if *debugFlag {
		fmt.Fprintf(os.Stdout, "%s\n", list[*lastIndex])
	}
	return list[*lastIndex]
}

func checkError(err error) {
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
}
